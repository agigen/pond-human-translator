#!/usr/bin/env python
#coding=utf8

import logging

from controllers import application
from webapp2_extras.routes import HandlerPrefixRoute, PathPrefixRoute
from webapp2_extras.routes import RedirectRoute as Route

import auth
import config.application

class NotFoundHandler(application.RequestHandler):
    def get(self):
        self.response.set_status(404, "Not Found")
        self.template = '404.html'

webapp_config = {}
webapp_config['webapp2_extras.sessions'] = {
    'secret_key': config.application.secret_key('session'),
    'cookie_args': {
        'httponly': True,
        'secure': config.application.secure_cookie
    }
}

app = application.webapp2.WSGIApplication([
    # default
    HandlerPrefixRoute('controllers.default.', [
        HandlerPrefixRoute('index_controller.', [
            Route('/api', 'IndexHandler', 'api-index', strict_slash=True),
            Route('/', 'IndexHandler', 'index', strict_slash=True),
        ]),

    ]),

    HandlerPrefixRoute('controllers.default.', [
        HandlerPrefixRoute('index_controller.', [
        ]),
    ]),

    # api
    HandlerPrefixRoute('controllers.api.', [
        HandlerPrefixRoute('api_controller.', [
            Route('/api/user', handler='TranslatorHandler'),
            Route('/api/user/<phone_number>', handler='GetOrUpdateTranslatorHandler'),
            Route('/api/search', handler='SearchHandler'),
            Route('/api/get_token', handler='GetApiTokenHandler'),
        ]),
    ]),

    # admin
    HandlerPrefixRoute('controllers.admin.', [
        HandlerPrefixRoute('index_controller.', [
            Route('/admin', handler='IndexHandler', name="admin-index", strict_slash=True),
            Route('/admin/sidebar', handler='SidebarHandler', name="admin-sidebar", strict_slash=True),
        ]),
    ]),

    (r'/login', auth.LoginHandler),
    (r'/.+', NotFoundHandler), # must be routed last
], config=webapp_config)
