#!/usr/bin/env python
#coding=utf8

import logging
import webapp2
import datetime
from translator.models import Translator

from controllers import application
import auth

class IndexHandler(application.RequestHandler):
    def get(self):
        self.template = 'public/index.html'
