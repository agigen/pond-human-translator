#!/usr/bin/env python
#coding=utf8

import logging
import webapp2
import pycountry
import datetime
import json

from controllers import application

from translator.models import Translator
from google.appengine.api import search
from google.appengine.ext import ndb

import auth

class TranslatorHandler(application.APIRequestHandler):
    def post(self):
        data = json.loads(self.request.body)

        if 'token' not in data or not auth.Auth.verify(data['token']):
            self.status = 'error'
            self.status_code = 403
            self.data['message'] = 'Failed to verify token'
            return

        if Translator.check_token_status_in_use(data['token']):
            self.status = 'error'
            self.status_code = 409
            self.data['message'] = 'The token is already in use'
            return

        try:
            Translator.create_translator(data['name'], data['phone_number'], data['translate_to'], data['translate_from'], data['location'], data['token'])
            self.data['message'] = 'User created succesfully'
        except Exception, e:
            self.status_code = 400
            self.data['message'] = 'Error creataing user, please check all fields'
            self.status = 'error'

class GetOrUpdateTranslatorHandler(application.APIRequestHandler):
    def get(self, phone_number):
        t = Translator.get_by_phone(phone_number)
        if not t:
            self.status = 'error'
            self.status_code = 404
            self.data['message'] = 'User not found'
            return

        self.data['message'] = 'User exists'

    def put(self, phone_number):
        data = json.loads(self.request.body) if self.request.body else None

        if not data:
            self.status = 'error'
            self.status_code = 404
            self.data['message'] = 'No data found'
            return

        if 'token' not in data or not auth.Auth.verify(data['token']):
            self.status = 'error'
            self.status_code = 403
            self.data['message'] = 'Failed to verify token'
            return

        t = Translator.get_by_phone(phone_number, data['token'])
        if not t:
            self.status = 'error'
            self.status_code = 404
            self.data['message'] = 'User not found'
            return

        translator = t[0] #get the first sice we only want that one

        for key, value in data.iteritems():
            if key == 'token':
                continue
            if key == 'translate_to' or key == 'translate_from':
                for lang in value:
                    if not pycountry.languages.get(iso639_1_code=lang):
                        self.status = 'error'
                        self.status_code = 400
                        self.data['message'] = 'One or more of the translation codes seems wrong'
                        return
            if key == 'location':
                if not pycountry.countries.get(alpha2=value.upper()):
                    self.status = 'error'
                    self.status_code = 400
                    self.data['message'] = 'The users locations is in the wrong format'
                    return

            setattr(translator, key, value)

        translator.put()
        self.data['message'] = 'User updated succesfully'

class SearchHandler(application.APIRequestHandler):
    def get(self):
        translate_from = self.request.get('translate_from')
        translate_to = self.request.get('translate_to')

        if not translate_from or not translate_to:
            self.status = 'error'
            self.data['message'] = 'You have to pass both translate_to and translate_from'
            self.status_code = 400
            return

        translators = Translator.search_by_languages(translate_from, translate_to)
        if len(translators):
            self.data['translators'] = [t.serialize() for t in translators]
        else:
            self.status = 'error'
            self.status_code = 404
            self.data['message'] = 'No translator available'

class GetApiTokenHandler(application.APIRequestHandler):
    def get(self):
        ssid = auth._generate_session_key()
        a = auth.Auth(
            id=ssid,
            ssid=ssid,
            verified=True
        )
        a.put()
        self.data['token'] = ssid

