#!/usr/bin/env python
#coding=utf8

import logging
import pycountry
from google.appengine.ext import ndb

import config.application

class Translator(ndb.Model):
    name = ndb.StringProperty()
    phone_number = ndb.StringProperty()
    translate_to = ndb.StringProperty(repeated=True)
    translate_from = ndb.StringProperty(repeated=True)
    location = ndb.StringProperty()
    available = ndb.BooleanProperty(default=True)

    token = ndb.StringProperty()

    created_at = ndb.DateTimeProperty(auto_now_add=True)
    updated_at = ndb.DateTimeProperty(auto_now=True)

    @classmethod
    def create_translator(cls, name, phone_number, translate_to, translate_from, location, token):
        logging.info('creating user...')
        for to in translate_to:
            if not pycountry.languages.get(iso639_1_code=to):
                raise Exception('Language not found translate_to %s ' % to)

        for from_lang in translate_from:
            if not pycountry.languages.get(iso639_1_code=from_lang):
                raise Exception('Language not found translate_from %s' % from_lang)

        if not pycountry.countries.get(alpha2=location.upper()):
            raise Exception('Country not found  %s' % location)

        t = Translator(name=name,
                phone_number=phone_number,
                translate_to=translate_to,
                translate_from=translate_from,
                location=location,
                token=token)
        t.put()
        return t

    @classmethod
    def get_by_phone(cls, phone_number, token=False):
        if token:
            t = cls.query(cls.phone_number==phone_number, cls.token==token).fetch(1)
        else:
            t = cls.query(cls.phone_number==phone_number).fetch(1)

        return t

    @classmethod
    def search_by_languages(cls, translate_from, translate_to):
        t = cls.query(cls.available==True).filter(cls.translate_from.IN([translate_from]), cls.translate_to.IN([translate_to])).fetch(100)
        return t

    @classmethod
    def check_token_status_in_use(cls, token):
        t = cls.query(cls.token==token).fetch(1)
        if len(t):
            return True
        return False

    def serialize(self):
        return {
            'name': self.name,
            'phone_number': self.phone_number,
            'translate_to': self.translate_to,
            'translate_from': self.translate_from,
            'location': self.location,
            'available': self.available
        }

